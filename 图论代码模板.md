图论代码模板

## LCA（最近公共祖先）

```c++
int depth[N], fa[N][log(N)];
void bfs(int root)  // 预处理倍增数组
{
    memset(depth, 0x3f, sizeof depth);
    depth[0] = 0, depth[root] = 1;  // depth存储节点所在层数
  	queue<int> q;
    q.push(root);
    while (q.size())
    {
        auto t = q.front(); q.pop();
        for (int i = h[t]; ~i; i = ne[i])
        {
            int j = e[i];
            if (depth[j] > depth[t] + 1)
            {
                depth[j] = depth[t] + 1;
                q.push(j);
                fa[j][0] = t;  // j的第二次幂个父节点
                for (int k = 1; k <= log(N); k ++ )
                    fa[j][k] = fa[fa[j][k - 1]][k - 1];
            }
        }
    }
}

int lca(int a, int b)  // 返回a和b的最近公共祖先
{
    if (depth[a] < depth[b]) swap(a, b);
    for (int k = log(N); k >= 0; k -- )
        if (depth[fa[a][k]] >= depth[b])
            a = fa[a][k];
    if (a == b) return a;
    for (int k = log(N); k >= 0; k -- )
        if (fa[a][k] != fa[b][k])
        {
            a = fa[a][k];
            b = fa[b][k];
        }
    return fa[a][0];
}
```

## tarjan离线求lca

```c++
int dist[N];
int p[N];
int res[M];
int st[N];

void dfs(int u, int fa)
{
    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];
        if (j == fa) continue;
        dist[j] = dist[u] + w[i];
        dfs(j, u);
    }
}

void tarjan(int u)
{
    st[u] = 1;
    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];
        if (!st[j])
        {
            tarjan(j);
            p[j] = u;
        }
    }

    for (auto item : query[u])
    {
        int y = item.first, id = item.second;
        if (st[y] == 2)
        {
            int anc = find(y);
            res[id] = dist[u] + dist[y] - dist[anc] * 2;
        }
    }

    st[u] = 2;
}

//  询问建双向边
for (int i = 0; i < m; i ++ )
{
    int a, b;
    scanf("%d%d", &a, &b);
    if (a != b)
    {
     	query[a].push_back({b, i});
        query[b].push_back({a, i});
    }
}
```



## 匈牙利算法(不带权最大匹配)

在二分图中

最小点覆盖 = 最大匹配

最大独立集  = 总点数 - 最大匹配（最小点覆盖）

dag中的最小路径点覆盖 = 总点数 - 最大匹配

dag中的最小重复路径点覆盖 = 总点数 - 传递闭包后的 最大匹配

```c++
bool st[N];
int match[N];

bool find(int x)
{
    for (int i = h[x]; i != -1; i = ne[i])
    {
        int j = e[i];
        if (!st[j])
        {
            st[j] = true;
            if (match[j] == 0 || find(match[j]))
            {
                match[j] = x;
                return true;
            }
        }
    }
    return false;
}

for (int i = 1; i <= n; i ++ )
{
    memset(st, 0, sizeof st);
    if (find(i)) ans ++ ;
}
```

## 染色法判定二分图

```c++
bool dfs(int u, int c)
{
    color[u] = c;

    for (int i = h[u]; i != -1; i = ne[i])
    {
        int j = e[i];
        if (!color[j])
        {
            if (!dfs(j, 3 - c)) return false;
        }
        else if (color[j] == c) return false;
    }

    return true;
}
```

## 堆优化版dijkstra(稀疏图)

```c++
int dijkstra()
{
    memset(dist, 0x3f, sizeof dist);
    dist[1] = 0;
    priority_queue<PII, vector<PII>, greater<PII>> heap;
    heap.push({0, 1});

    while (heap.size())
    {
        auto t = heap.top();
        heap.pop();

        int ver = t.second, distance = t.first;

        if (st[ver]) continue;
        st[ver] = true;

        for (int i = h[ver]; i != -1; i = ne[i])
        {
            int j = e[i];
            if (dist[j] > dist[ver] + w[i])
            {
                dist[j] = dist[ver] + w[i];
                heap.push({dist[j], j});
            }
        }
    }

    if (dist[n] == 0x3f3f3f3f) return -1;
    return dist[n];
}
```

## 朴素版dijkstra(稠密图)

```c++
void dijkstra(int bg)
{
    memset(dist, 0x3f3f3f3f, sizeof dist);
    
    dist[bg] = 0;
    
    for (int i = 1; i <= n - 1; i ++ )
    {
        int t = -1;
        
        for (int j = 1; j <= n; j ++ )
            if (!st[j] && (t == -1 || dist[t] > dist[j]))
                t = j;
        
        st[t] = true;
        
        for (int j = 1; j <= n; j ++ )
            dist[j] = min(dist[j],  dist[t] + g[t][j]);
    }
}

memset(g, 0x3f, sizeof g);
g[a][b] = min(g[a][b], c);  // 考虑重边
```

## bellman-ford算法

```c++
struct Node
{
    int a, b, c;
}edges[M];

int dist[N], last[N];

void bellman_ford()
{
    memset(dist, 0x3f, sizeof dist);
    dist[1] = 0;

    for (int i = 0; i < k; i ++ )
    {
        memcpy(last, dist, sizeof dist);

        for (int j = 1; j <= m; j ++ )
        {
            auto e = edges[j];
            dist[e.b] = min(dist[e.b], last[e.a] + e.c);
        }
    }
}

signed main()
{
    cin >> n >> m >> k;

    for (int i = 1; i <= m; i ++ )
    {
        int a, b, c;
        cin >> a >> b >> c;

        edges[i] = {a, b, c};
    }

    bellman_ford();

    if (dist[n] >  INF / 2) cout << "impossible" << endl;
    else cout << dist[n] << endl;

    please ac;
}
```

## spfa

```c++
int dist[N];
bool st[N];

void spfa(int bg)
{
    queue<int> q;

    memset(dist, 0x3f, sizeof dist);
    q.push(bg);
    dist[bg] = 0;
    st[bg] = true;

    while (q.size())
    {
        auto t = q.front(); q.pop();

        st[t] = false;

        for (int i = h[t]; ~i; i = ne[i])
        {
            int j = e[i];

            if (dist[j] > dist[t] + w[i])
            {
                dist[j] = dist[t] + w[i];

                if (!st[j]) q.push(j), st[j] = true;
            }
        }
    }
}
```

## spfa算法判负环

```c++
int dist[N], cnt[N];
bool st[N];

bool spfa()
{
    queue<int> q;

    for (int i = 1; i <= n; i ++ ) q.push(i), st[i] = true;

    while (q.size())
    {
        auto a = q.front(); q.pop();

        st[a] = false;

        for (int i = h[a]; ~i; i = ne[i])
        {
            int j = e[i];

            if (dist[j] > dist[a] + w[i])
            {
                dist[j] = dist[a] + w[i];
                cnt[j] = cnt[a] + 1;

                if (cnt[j] >= n) return true;

                if (!st[j])
                {
                    st[j] = true;
                    q.push(j);
                }
            }
        }
    }

    return false;
}
```

若发现spfa求负环会t，这里有个trick，可以把这里的队列换成栈

## floyd算法

```c++
int g[N][N];

void init()
{
    memset(g, 0x3f, sizeof g);

    for (int i = 1; i <= n; i ++ ) g[i][i] = 0; // 很重要

    // 判不连通 t > INF / 2
    // 重边： g[a][b] = g[b][a] = min(c, g[a][b]);
}

void floyd()
{
    for (int k = 1; k <= n; k ++ )
        for (int i = 1; i <= n; i ++ )
            for (int j = 1; j <= n; j ++ )
                g[i][j] = min(g[i][j], g[i][k] + g[k][j]);
}
```

## prim算法（稠密图）

```c++
int prim(int root)
{
    memset(dist, 0x3f, sizeof dist);

    int res = 0;

    dist[root] = 0;

    for (int i = 0; i < n; i ++ )
    {
        int t = -1;

        for (int j = 1; j <= n; j ++ )
            if (!st[j] && (t == -1 || dist[j] < dist[t]))
                t = j;

        if (i && dist[t] == INF) return INF;
        if (i) res += dist[t];

        st[t] = true;

        for (int j = 1; j <= n; j ++ )
            dist[j] = min(dist[j], g[t][j]);
    }

    return res;
}

memset(g, 0x3f, sizeof g);
g[a][b] = g[b][a] = min(c, g[b][a]);

```

## kruskal算法(稀疏图)

```c++
int kruskal()
{
    sort(edges + 1, edges + 1 + m);

    int res = 0;

    for (int i = 1; i <= m; i ++ )
    {
        auto e = edges[i];

        int pa = find(e.a), pb = find(e.b);

        if (pa != pb)
        {
            res += e.w;
            n -- ;
            p[pa] = pb;
        }
    }

    if (n != 1) return INF;
    return res;
}
```

## tarjan(强连通分量)

```c++
int dfn[N], low[N], timestamp;
int Size[N], id[N], scc_cnt;
int stk[N], top;
bool in_stk[N];

void tarjan(int u)
{
    dfn[u] = low[u] = ++ timestamp;
    stk[ ++ top] = u, in_stk[u] = true;

    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];

        if (!dfn[j])
        {
            tarjan(j);
            low[u] = min(low[u], low[j]);
        }
        else if (in_stk[j]) low[u] = min(low[u], dfn[j]);
    }

    if (dfn[u] == low[u])
    {
        ++ scc_cnt;
        int y;

        do
        {
            y = stk[top -- ];
            in_stk[y] = false;
            id[y] = scc_cnt;
            Size[scc_cnt] ++ ;
        } 
        while (y != u);
    }
}
```

## tarjan(边双连通分量)

```c++
int dfn[N], low[N], timestamp;
int stack[N], top;
int id[N], dcc_cnt;
bool is_bridge[M];

void tarjan(int u, int from)
{
    dfn[u] = low[u] = timestamp ++ ;
    stack[ ++ top] = u;

    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];

        if (!dfn[j])
        {
            tarjan(j, i);
            low[u] = min(low[u], low[j]);

            if (low[j] > dfn[u]) is_bridge[i] = is_bridge[i ^ 1] = true;
        }
        else if (i != (from ^ 1)) low[u] = min(dfn[j], low[u]);
    }

    if (dfn[u] == low[u])
    {
        dcc_cnt ++ ;
        int y;
        do
        {
            y = stack[top -- ];
            id[y] = dcc_cnt;
        } 
        while (y != u);
    }
}
```

## tarjan(点双连通分量)

```c++
int dfn[N], low[N], timestamp;
int stk[N], top;
bool cut[N];
vector<int> dcc[N];
int dcc_cnt;
int root;

void tarjan(int u)
{
    dfn[u] = low[u] = ++ timestamp;
    stk[ ++ top] = u;

    if (u == root && h[u] == -1)
    {
        dcc_cnt ++ ;
        dcc[dcc_cnt].push_back(u);
        return ;
    }

    int cnt = 0;

    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];

        if (!dfn[j])
        {
            tarjan(j);
            low[u] = min(low[u], low[j]);

            if (low[j] >= dfn[u])
            {
                cnt ++ ;
                if (u != root || cnt > 1) cut[u] = true;

                ++ dcc_cnt;
                int y;

                do
                {
                    y = stk[top -- ];
                    dcc[dcc_cnt].push_back(y);
                }
                while (y != j);
                dcc[dcc_cnt].push_back(u);
            }
        }
        else low[u] = min(low[u], dfn[j]);
    }
}
```

## 关于将整个图补成连通分量

强连通分量：max(p, q) p 和 q代表出度和入度为0的连通分量

边双连通分量：cnt + 1 >> 1 cnt代表度为1的联通分量

## 关于图论中的01分数规划

利用二分整理式子来进行check`

## 差分约束

求最小值就求最长路 把不等式变成  a >= b + c 从b给a连一条长度为c的边

求最大值就求最短路 把不等式变成  a <= b + c 从b给a连一条长度为c的边

并对常量进行超级源点连边。

最后进行判负环或正环对应着最短路和最长路有没有解



## 次小生成树（此代码为严格）

```c++
int p[N];
int find(int x)
{
    if (p[x] != x) p[x] = find(p[x]);
    return p[x];
}

void init()
{
    iota(p + 1, p + 1 + N, 1);
}

struct Node
{
    int a, b, w;
    bool f;

    bool operator< (const Node &t)const
    {
        return w < t.w;
    }
}edges[M];

int kruskal()
{
    init();
    sort(edges + 1, edges + 1 + m);
    memset(h, -1, sizeof h);

    for (int i = 1; i <= m; i ++ )
    {
        auto &e = edges[i];

        int pa = find(e.a), pb = find(e.b);

        if (pa != pb)
        {
            sum += e.w;
            p[pa] = pb;
            e.f = true;

            add(e.a, e.b, e.w), add(e.b, e.a, e.w);
        }
    }

}

int depth[N];
int fa[N][17], d1[N][17], d2[N][17];

void bfs()
{
    queue<int> q;
    q.push(1);

    memset(depth, 0x3f, sizeof depth);

    depth[0] = 0;
    depth[1] = 1;

    while (q.size())
    {
        auto t = q.front(); q.pop();

        for (int i = h[t]; ~i; i = ne[i])
        {
            int j = e[i];
            
            if (depth[j] > depth[t] + 1)
            {
                depth[j] = depth[t] + 1;
                q.push(j);

                fa[j][0] = t;
                d1[j][0] = w[i], d2[j][0] = -INF;

                for (int k = 1; k <= 16; k ++ )
                {
                    int anc = fa[j][k - 1];
                    fa[j][k] = fa[anc][k - 1];

                    int distance[4] = {d1[j][k - 1], d2[j][k - 1],
                                       d1[anc][k - 1], d2[anc][k - 1]};
                    d1[j][k] = d2[j][k] = -INF;

                    for (int u = 0; u < 4; u ++ )
                    {
                        int d = distance[u];

                        if (d > d1[j][k]) d2[j][k] = d1[j][k], d1[j][k] = d;
                        else if (d < d1[j][k] && d2[j][k] < d) d2[j][k] = d;
                    }
                }
            }
        }
    }
}

LL lca(int a, int b, int w)
{
    int cnt = 0;
    static int distance[N << 1];

    if (depth[a] < depth[b]) swap(a, b);

    for (int k = 16; k >= 0; k -- )
        if (depth[fa[a][k]] >= depth[b])
        {
            distance[cnt ++ ] = d1[a][k];
            distance[cnt ++ ] = d2[a][k];
            a = fa[a][k];
        }

    if (a != b)
    {
        for (int k = 16; k >= 0; k -- )
            if (fa[a][k] != fa[b][k])
            {
                distance[cnt ++ ] = d1[a][k];
                distance[cnt ++ ] = d2[a][k];
                distance[cnt ++ ] = d1[b][k];
                distance[cnt ++ ] = d2[b][k];   

                b = fa[b][k];
                a = fa[a][k];
            }

        distance[cnt ++ ] = d1[a][0];
        distance[cnt ++ ] = d2[b][0];
        distance[cnt ++ ] = d1[b][0];
        distance[cnt ++ ] = d2[a][0];
    }

    int dist1 = -INF, dist2 = -INF;

    for (int i = 0; i < cnt; i ++ )
    {
        int d = distance[i];

        if (dist1 < d) dist2 = dist1, dist1 = d;
        else if(dist1 > d && dist2 < d) dist2 = d;
    }

    if (w > dist1) return w - dist1;
    if (w > dist2) return w - dist2;

    return INF;

}

signed main()
{
    ios::sync_with_stdio(false), cin.tie(0), cout.tie(0);
    
    cin >> n >> m;

    for (int i = 1; i <= m; i ++ )
    {
        int a, b, c;
        cin >> a >> b >> c;

        edges[i] = {a, b, c};
    }

    kruskal();
    bfs();

    for (int i = 1; i <= m; i ++ )
    {
        if (edges[i].f) continue;
        auto e = edges[i];

        ans = min(ans, sum + lca(e.a, e.b, e.w));
    }

    cout << ans << endl;

    return 0;
}
```

## 欧拉回路/路径

欧拉回路：

首先所有边都联通

有向图：所有点入度等于出度(回路)，除了俩个点以外入度等于出度（路径），那俩个点一个出 = 入 + 1， 入 = 出 + 1；

无向图：所有点的度数为偶数(回路)， 奇数点有且只有俩个(路径)

在有欧拉路径的情况下：

```c++
bool used[M];
int res[M], cnt;
int din[N], dout[N];

void dfs(int u)
{
    for (int &i = h[u]; ~i;)
    {
        if (used[i])
        {
            i = ne[i];
            continue;
        }

        if (type == 1) used[i ^ 1] = true;
        
        int t = w[i];
        int j = e[i];
        i = ne[i];

        dfs(j);

        res[ ++ cnt] = t;
    }
}

if (cnt < m) //  判断有没有孤立点

for (int i = cnt; i; i -- ) cout << res[i] << ' ';  //  反着输出
```

## 拓扑排序

```c++
int d[N], q[N];

bool topSort()
{
    int hh = 0, tt = -1;
    
    for (int i = 1; i <= n; i ++ )
        if (!d[i]) q[ ++ tt] = i;
    
    while (hh <= tt)
    {
        auto u = q[hh ++ ];
        
        for (int i = h[u]; ~i; i = ne[i])
        {
            int j = e[i];
            
            if ( -- d[j] == 0) q[ ++ tt] = j;
        }
    }
    
    return n == tt + 1;
}
```

## 朱刘算法

### 稠密图

```c++
void tarjan(int u)
{
    dfn[u] = low[u] = ++ timestamp;
    stk[ ++ top] = u;
    in_stk[u] = true;

    int j = pre[u];

    if (!dfn[j])
    {
        tarjan(j);

        low[u] = min(low[j], low[u]);
    }
    else if (in_stk[j]) low[u] = min(dfn[j], low[u]);

    if (low[u] == dfn[u])
    {
        int y;
        scc_cnt ++ ;

        do
        {
            y = stk[top -- ];
            in_stk[y] = false;
            id[y] = scc_cnt;
        }
        while (y != u);
    }
}

int zhuliu()
{
    double res = 0;

    while (true)
    {
        for (int i = 1; i <= n; i ++ )
        {
            pre[i] = i;
            
            for (int j = 1; j <= n; j ++ )
                if (d[pre[i]][i] > d[j][i])
                    pre[i] = j;
        }

        memset(dfn, 0, sizeof dfn);
        timestamp = scc_cnt = 0;

        for (int i = 1; i <= n; i ++ )
            if (!dfn[i]) tarjan(i);
        
        if (scc_cnt == n) 
        {
            for (int i = 2; i <= n; i ++ ) res += d[pre[i]][i];
            break;
        }

        for (int i = 2; i <= n; i ++ )
            if (id[pre[i]] == id[i])
                res += d[pre[i]][i];
        
        for (int i = 1; i <= scc_cnt; i ++ )
            for (int j = 1; j <= scc_cnt; j ++ )
                bd[i][j] = INF;

        for (int i = 1; i <= n; i ++ )
            for (int j = 1; j <= n; j ++ )
                if (d[i][j] < INF && id[i] != id[j])
                {
                    int a = id[i], b = id[j];

                    if (id[pre[j]] == id[j]) bd[a][b] = 
                        min(bd[a][b], d[i][j] - d[pre[j]][j]);
                    else bd[a][b] =  min(bd[a][b], d[i][j]);
                }

        n = scc_cnt;
        memcpy(d, bd, sizeof d);
    }
    return res;
}

signed main()
{
    scanf("%d", &T);

    for (int i = 1; i <= T; i ++ )
    {
        scanf("%d%d", &n, &m);

        memset(d, 0x3f, sizeof d);

        while (m -- )
        {
            int a, b, c;
            scanf("%d%d%d", &a, &b, &c);

            a ++ , b ++ ;

            if (b != 1) d[a][b] = c;
        }
        
        if (!check_con()) printf("Case #%d: Possums!\n", i);
        else printf("Case #%d: %d\n", i, zhuliu());
    }

    return 0;
}
```

### 稀疏图

```c++
int pre[N], minv[N];
int stk[N], top;
bool in_stk[N];
int dfn[N], low[N], timestamp;
int id[N], scc_cnt;
int root;
struct Edge { int u, v, w; } edges[M], temp[M];

void tarjan(int u)
{
    dfn[u] = low[u] = ++timestamp;
    stk[++top] = u, in_stk[u] = true;

    int v = pre[u];
    if (!dfn[v]) {
        tarjan(v);
        low[u] = min(low[u], low[v]);
    }
    else if (in_stk[v])
        low[u] = min(low[u], dfn[v]);

    if (dfn[u] == low[u]) {
        scc_cnt++;
        do {
            v = stk[top--];
            in_stk[v] = false;
            id[v] = scc_cnt;
        } while (u != v);
    }
}

int zhuliu()
{
    int ans = 0;
    while (true) {
        memset(minv, 0x3f, sizeof minv);
        for (int i = 1; i <= n; i++) pre[i] = i;

        for (int i = 0; i < m; i++) {
            int u = edges[i].u, v = edges[i].v, w = edges[i].w;
            if (v == root) continue;
            if (minv[v] > w)
                pre[v] = u, minv[v] = w;
        }

        for (int i = 1; i <= n; i++)
            if (i != root && minv[i] > INF / 2) return INF;

        memset(dfn, 0, sizeof dfn);
        timestamp = scc_cnt = 0;

        for (int i = 1; i <= n; i++)
            if (!dfn[i]) tarjan(i);

        if (scc_cnt == n) {
            for (int i = 1; i <= n; i++)
                if (i != root) ans += minv[i];
            break;
        }

        for (int i = 1; i <= n; i++)
            if (i != root && id[pre[i]] == id[i])
                ans += minv[i];

        int k = 0;
        for (int i = 0; i < m; i++) {
            int u = edges[i].u, v = edges[i].v, w = edges[i].w;
            if (id[u] != id[v]) {
                int a = id[u], b = id[v];
                if (id[pre[v]] == id[v]) temp[k++] = {a, b, w - minv[v]};
                else temp[k++] = {a, b, w};
            }
        }

        n = scc_cnt, m = k;
        root = id[root];
        memcpy(edges, temp, sizeof temp);
    }

    return ans;
}

int main()
{
    ios::sync_with_stdio(false);
    int cases;
    cin >> cases;

    for (int c = 1; c <= cases; c++) {
        cin >> n >> m;

        root = 1;
        for (int i = 0; i < m; i++) {
            int a, b, c;
            cin >> a >> b >> c;
            a++, b++;
            // @todo 解决重边
            edges[i] = {a, b, c};
        }

        int ans = zhuliu();

        printf("Case #%d: ", c);
        if (ans > INF / 2) puts("Possums!");
        else printf("%ld\n", ans);
    }

    return 0;
}
```

## 网络流

### EK求最大流

```c++
int d[N], pre[N];
bool st[N];
int S, E;

bool bfs()
{
    queue<int> q;
    memset(st, false, sizeof st);
    q.push(S);
    st[S] = true;
    d[S] = INF;

    while (q.size())
    {
        auto t = q.front(); q.pop();

        for (int i = h[t]; ~i; i = ne[i])
        {
            int j = e[i];

            if (!st[j] && w[i] > 0)
            {
                st[j] = true;
                d[j] = min(w[i], d[t]);
                pre[j] = i;
                if (j == E) return true;
                q.push(j);
            }
        }
    }

    return false;
}

int EK()
{
    int r = 0;

    while (bfs())
    {
        r += d[E];
        
        for (int i = E; i != S; i = e[pre[i] ^ 1])
            w[pre[i]] -= d[E], w[pre[i] ^ 1] += d[E];
    }
    
    return r;
}

signed main()
{
    memset(h, -1, sizeof h);

    cin >> n >> m >> S >> E;

    for (int i = 1; i <= m; i ++ )
    {
        int a, b, c;
        cin >> a >> b >> c;

        add(a, b, c), add(b, a, 0);
    }

    cout << EK() << endl;

    please ac;
}
```

### dinic求最大流

```c++
int d[N], cur[N];

int S, E;

bool bfs()
{
    queue<int> q;
    memset(d, -1, sizeof d);

    q.push(S);
    d[S] = 0;
    cur[S] = h[S];

    while (q.size())
    {
        auto t = q.front(); q.pop();

        for (int i = h[t]; ~i; i = ne[i])
        {
            int j = e[i];

            if (d[j] == -1 && w[i])
            {
                d[j] = d[t] + 1;
                cur[j] = h[j];
                if (j == E) return true;
                q.push(j);
            }   
        }
    }
    return false;
}

int find(int u, int limit)
{
    if (u == E) return limit;
    int flow = 0;

    for (int i = cur[u]; ~i && flow < limit; i = ne[i])
    {
        cur[u] = i;

        int j = e[i];

        if (d[j] == d[u] + 1 && w[i])
        {   
            int t = find(j, min(w[i], limit - flow));
            if (!t) d[j] = -1;
            w[i] -= t, w[i ^ 1] += t, flow += t;
        }
    }

    return flow;
}

int dinic()
{
    int r = 0, flow;

    while (bfs()) while (flow = find(S, INF)) r += flow;

    return r;
}

signed main()
{
    cin >> n >> m >> S >> E;

    memset(h, -1, sizeof h);

    while (m -- )
    {
        int a, b, c;
        cin >> a >> b >> c;

        add(a, b, c), add(b, a, 0);
    }

    cout << dinic() << endl;
}
```

### 无源汇上下界可行流建图方法

把这个问题转化成新图上的满流的最大流问题

新图：

每个边的容量 =  上界 - 下界

这样就会使流量不守恒，使用源点来补齐这些缺失的流量，然后跑一边dinic，必须要满流

反向边的容量 + 下界就是一个可行流的流量

```c++
int h[N], e[M], w[M], l[M], ne[M], idx;
void add(int a, int b, int c, int d)
{
    e[idx] = b, w[idx] = d - c, l[idx] = c, ne[idx] = h[a], h[a] = idx ++ ;
    e[idx] = a, w[idx] = 0, ne[idx] = h[b], h[b] = idx ++ ;
}

for (int i = 1; i <= m; i ++ )
    {
        int a, b, c, d;
        cin >> a >> b >> c >> d;

        add(a, b, c, d);

        A[a] -= c, A[b] += c;
    }

    for (int i = 1; i <= n; i ++ )
        if (A[i] > 0) add(S, i, 0, A[i]), tot += A[i];
        else if (A[i] < 0) add(i, E, 0, -A[i]);


    if (dinic() != tot) cout << "NO" << endl;
    else 
    {
        cout << "YES" << endl;
        for (int i = 0; i < m * 2; i += 2)
            cout << w[i ^ 1] + l[i] << endl;
    }
```

### 有源汇上下界最大/最小流建图

首先也把这个问题转化成新图上的最大流问题，这样就可以得到一个可行流

新图：

每个边的容量 =  上界 - 下界

这样就会使流量不守恒，使用源点来补齐这些缺失的流量，然后跑一边dinic，必须要满流

反向边的容量 + 下界就是一个可行流的流量

在这基础上我们要对图上原本的源点和汇点再求一遍最大流

答案就是俩次最大流相加最大流

最小流就反向求最大流，然后res - 最大流就是最小流

比较重要的是要建一条原本的t - > s 有一个正无穷的边，因为源点是可以源源不断流的

```c++
signed main()
{
    ios::sync_with_stdio(false), cin.tie(0), cout.tie(0);
    cout << fixed <<  setprecision(0);

    cin >> n >> m >> s >> t;

    S = 0, T = n + 1;
    int tot = 0;

    memset(h, -1, sizeof h);

    while (m -- )
    {
        int a, b, c, d;

        cin >> a >> b >> c >> d;

        A[a] -= c, A[b] += c;

        add(a, b, d - c), add(b, a, 0);
    }

    for (int i = 1; i <= n; i ++ )
        if (A[i] > 0) add(S, i, A[i]), add(i, S, 0), tot += A[i];
        else if (A[i] < 0) add(i, T, -A[i]), add(T, i, 0);

    add(t, s, INF), add(s, t, 0);

    if (dinic() != tot) cout << "No Solution" << endl;
    else 
    {
        int res = w[idx - 1];

        w[idx - 1] = w[idx - 2] = 0;
        S = t, T = s;
        cout << res - dinic() << endl;
    }
}
```

### 最大权闭合图建边（最小割）

把负的点权连到汇点E（边权为绝对值）

S连到正的点权

最大权闭合图的权值为 正的点权 - 最小割

内部的边连成正无穷

方案：

S可以扩展到的点就是方案  	

```c++
signed main()
{
    cin >> m >> n;

    S = 0, E = n + m + 1;
    int tot = 0;

    memset(h, -1, sizeof h);

    for (int i = 1; i <= m; i ++ )
    {
        int a;
        cin >> a;

        add(i + n, E, a);
    } 

    for (int i = 1; i <= n; i ++ )
    {
        int a, b, c;
        cin >> a >> b >> c;

        add(S, i, c);

        add(i, a + n, INF);
        add(i, b + n, INF);

        tot += c;
    }

    cout << tot - dinic() << endl;

    please ac;
}
```

### 最大密度子图

利用01分数规划可知

e + v / v = g => e(边权) + v(点权) - v(点数)g   最小化 (g)v  - (v + e),  gv + kc(u, v)  所以我们可以像下图一样连边，求出最小割

![image-20220321180615778](https://jin-xuanjie.gitee.io/note-pad/image-20220321180615778.png)

观察以上式子，为啥dv也就是边权除了2呢，是因为我们计算入边和出边时是一起计算的，所以算了俩遍

解决了 点权 + 边权的一个最值问题 

g不受变化

$d_v$是边权，边权是1或者v都一样，算入边和出边的和 / 2 就可以

$p_v$是点权，有点权就要有，没点权去掉就可以了

```c++
bool check(double mid)
{
    memset(h, -1, sizeof h);
    idx = 0;

    for (int i = 1; i <= m; i ++ )
    {
        auto e = edges[i];

        add(e.x, e.y, 1, 1);
    }

    for (int i = 1; i <= n; i ++ )
    {
        add(S, i, m, 0);
        add(i, E, m + 2 * mid - dg[i], 0);
    }   

    double res = dinic();

    return m * n - res > 0;
}

void dfs(int u)
{
    st[u] = true;
    if (u != S) ans ++ ;
    for (int i = h[u]; ~i; i = ne[i])
    {
        int ver = e[i];
        if (!st[ver] && w[i] > 0)
            dfs(ver);
    }
}


signed main()
{
    ios::sync_with_stdio(false), cin.tie(0), cout.tie(0);
    cout << fixed <<  setprecision(15);

    cin >> n >> m;

    S = 0, E = n + 1;

    for (int i = 1; i <= m; i ++ )
    {
        int a, b;
        cin >> a >> b;

        edges[i] = {a, b};
        dg[a] ++ , dg[b] ++ ;
    }

    double l = 0, r = m;

    while (r - l > eps)
    {
        double mid = (r + l) / 2;   
        if (check(mid)) l = mid;
        else r = mid;
    }

    check(l);
    dfs(S);

    if (ans == 0) cout << 1 << endl << 1 << endl;
    else 
    {
        cout << ans << endl;

        for (int i = 1; i <= n; i ++ )
            if (st[i])
                cout << i << endl;
    }

    please ac;
}
```

### 最小点权覆盖

特指二分图：

分成二分图后，从S往一个分量连点权的边，另一个分量连点权往E连边

中间的关系连INF

最后最小割就是最小点覆盖

方案：

割边的俩点中除了S和E外就是选择的点

### 最大独立集合

总权值 - 最小点覆盖

### EK算法求最小/最大费用流

```c++
int d[N], pre[N], incf[N];
int S, E;
bool st[N];

bool spfa()
{
    queue<int> q;

    memset(d, 0x3f, sizeof d);
    memset(incf, 0, sizeof incf);
    q.push(S), d[S] = 0, incf[S] = INF;
    st[S] = true;

    while (q.size())
    {
        auto t = q.front(); q.pop();

        st[t] = false;

        for (int i = h[t]; ~i; i = ne[i])
        {
            int j = e[i];

            if (w[i] && d[j] > d[t] + f[i])
            {
                d[j] = d[t] + f[i];
                incf[j] = min(incf[t], w[i]);
                pre[j] = i;

                if (!st[j])
                {
                    st[j] = true;
                    q.push(j);
                }
            }
        }
    }

    return incf[E] > 0;
}

void EK(int &flow, int &cost)
{
    flow = cost = 0;

    while (spfa())
    {
        int t = incf[E];

        flow += t, cost += t * d[E];

        for (int i = E; i != S; i = e[pre[i] ^ 1])
        {
            w[pre[i]] -= t, w[pre[i] ^ 1] += t;
        }
    }
}
```

## 求第k短路

估价函数为到终点的最小值，利用dijkstra，反向建边跑一遍

然后再正着等弹出第k下时，就是第k短路

```c++
int astar()
{
    priority_queue<PIII, vector<PIII>, greater<PIII>> heap;

    heap.push({dist[S], {S, 0}});

    while (heap.size())
    {
        auto t = heap.top(); heap.pop();

        int distance = t.y.y;
        int ver = t.y.x;
        cnt[ver] ++ ;

        if (cnt[E] == k) return distance;

        for (int i = h[ver]; ~i; i = ne[i])
        {
            int j = e[i];

            if (cnt[j] < k)
            {
                heap.push({distance + w[i] + dist[j], {j, distance + w[i]}});
            }
        }
    }

    return -1;
}
```

