# 模板

## 背包问题

### 01背包

```c++
    for (int i = 1; i <= n; i ++ )
        for (int j = m; j >= v[i]; j -- )
            f[j] = max(f[j], f[j - v[i]] + w[i]);
```

### 完全背包

```c++
    for (int i = 1; i <= n; i ++ )
        for (int j = v[i]; j <= m; j ++ )
            f[j] = max(f[j], f[j - v[i]] + w[i]);
```

### 多重背包

```c++
 for (int i = 1; i <= n; i ++ )
        for (int j = 0; j <= m; j ++ )
            for (int k = 0; k <= s[i] && k * v[i] <= j; k ++ )
                f[i][j] = max(f[i][j], f[i - 1][j - v[i] * k] + w[i] * k);
```

#### 二进制优化

```c++
 for (int i = 1; i <= n; i ++ )
        for (int j = 0; j <= m; j ++ )
            for (int k = 0; k <= s[i] && k * v[i] <= j; k ++ )
                f[i][j] = max(f[i][j], f[i - 1][j - v[i] * k] + w[i] * k);
```

#### 单调队列优化

```c++
for (int i = 0; i < n; i ++ )
    {
        int v, w, s;
        cin >> v >> w >> s;
        memcpy(g, f, sizeof f);
        
        for (int j = 0; j < v; j ++ )
        {
            int hh = 0, tt = -1;
            for (int k = j; k <= m; k += v)
            {
                if (hh <= tt && q[hh] < k - s * v) hh ++ ;
                while (hh <= tt && g[q[tt]] - (q[tt] - j) / v * w <= g[k] - (k - j) / v * w) tt -- ;
                q[ ++ tt] = k;
                f[k] = g[q[hh]] + (k - q[hh]) / v * w;
            }
        }
    }
```

## 整数划分dp

### 完全背包的分析方式（总和为j的所有方案）

```c++
f[0] = 1;

for (int i = 1; i <= n; i ++ )
	for (int j = i; j <= n; j ++ )
		f[j] = (f[j] + f[j - i]) % mod;
```

### 独有的分析方式(总和为i, 个数为j的所有方案)

```c++
f[0][0] = 1;

for (int i = 1; i <= n; i ++ )
{
    for (int j = 1; j <= n; j ++ )
    {
     	f[i][j] = (f[i][j] + f[i - 1][j - 1]) % mod;
     	if (i >= j) f[i][j] = (f[i - j][j] + f[i][j]) % mod;
    }
}
```

## 最长公共子序列 (数组从1开始)

```c++
for (int i = 1; i <= n; i ++ )
        for (int j = 1; j <= m; j ++ )
        {
            f[i][j] = max(f[i - 1][j], f[i][j - 1]);
            f[i][j] = max(f[i - 1][j - 1] + (s1[i] == s2[j]), f[i][j]);
        }
```

还可以翻转一下求最长回文子序列

<hr>

## SOSDP

```c++
for (int i = 0; i < n; ++i)
{
    for (int j = 0; j < maxbit; ++j)
    {
        if (j & (1 << i))pre_sum[j] += pre_sum[j ^ (1 << i)];
        else suf_sum[j] += suf_sum[j ^ (1 << i)];
    }
}
```

先枚举每一位，后枚举所有状态。

presum是子集，suf_sum是超集

## 树上背包

#### 对于每个点的重量为1的树上背包$o(n * n)$

```c++
void dfs(int u)
{
    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];

        dfs(j);

        for (int k = 0; k <= sz[u] + sz[j]; k ++ ) tmp[k] = -INF;

        for (int _i = 0; _i <= sz[u]; _i ++ )
            for (int _j = 0; _j <= sz[j]; _j ++ )
                tmp[_i + _j] = max(tmp[_i + _j], f[u][_i] + f[j][_j]);

        for (int k = 0; k <= sz[u] + sz[j]; k ++ ) f[u][k] = tmp[k];
        sz[u] += sz[j];
    }

    sz[u] ++ ;

    for (int i = sz[u]; i > 0; i -- )
        f[u][i] = f[u][i - 1] + a[u];   

    f[u][0] = 0;
}
```

#### 每个点有重量 $o(n * m ^ 2)$

```c++
void dfs(int u)
{

    for (int i = h[u]; ~i; i = ne[i])
    {
        int j = e[i];

        dfs(j);

        for (int k = 0; k <= V; k ++ ) tmp[k] = -INF;

        for (int _i = 0; _i <= V; _i ++ )
            for (int _j = 0; _j <= V; _j ++ )
                    if (_i + _j <= V)
                        tmp[_i + _j] = max(tmp[_i + _j], f[u][_i] + f[j][_j]);
 
        for (int k = 0; k <= V; k ++ ) f[u][k] = tmp[k];
        sz[u] += sz[j];
    }

    sz[u] ++ ;

    for (int i = V; i >= b[u]; i -- )
        f[u][i] = f[u][i - b[u]] + a[u];   

    f[u][0] = 0;
    for (int i = 1; i < b[u]; i ++ )
        f[u][i] = 0; // -INF
}

int root;

void solve()
{
    init();

    memset(h, -1, sizeof h);
    // memset(f, -0x3f, sizeof f);

    cin >> n >> m;

    for (int i = 1; i <= n; i ++ )
    {
        int fa;
        cin >> b[i] >> a[i] >> fa;

        if (fa != -1) add(fa, i, 1);
        else root = i;
    }

    dfs(root);
 

    // cout << *max_element(f[root], f[root] + m + 1)  << endl;
    cout << f[root][m] << endl;
    // 相等与不大于的区别
}
```

#### 欧拉序求根节点的树上背包$(n * m)$

```c++
void dfs(int u)
{
    l[u] = ++ tot;
    id[tot] = u;

    for (int i = h[u]; ~i; i = ne[i])
        dfs(e[i]);

    r[u] = tot;
} 

void solve()
{
    init();

    memset(h, -1, sizeof h);

    cin >> n >> m;

    for (int i = 2; i <= n; i ++ )
    {
        int fa;
        cin >> fa;

        add(fa, i, 1);
    }

    for (int i = 1; i <= n; i ++ ) cin >> a[i];
    for (int i = 1; i <= n; i ++ ) cin >> b[i];

    dfs(1);
    
    memset(f[n + 1] + 1, -0x3f, 4 * (m));

    for (int i = tot; i >= 1; i -- )
    {
        int u = id[i];

        for (int j = 0; j <= m; j ++ )
        {
            f[i][j] = f[r[u] + 1][j];

            if (j >= b[u])
                f[i][j] = max(f[i][j], f[i + 1][j - b[u]] + a[u]);
        }
    }

    for (int i = 0; i <= m; i ++ )
        if (f[1][i] >= 0) cout << f[1][i] << endl;
        else cout << 0 << endl;
}
```

