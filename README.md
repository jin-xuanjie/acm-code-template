# acm代码模板

#### 介绍
**会分享本人的代码模板**

```c++
#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <set>
#include <cmath>
#include <sstream>
#include <bitset>
#include <ctime>
#include <stack>
#include <numeric>
#include <iomanip>
// #pragma GCC optimize(3,"Ofast","inline")

#define x first
#define y second
#define endl '\n'
#define please return
#define ac 0
// #define lc (u << 1)
// #define rc (u << 1 | 1)

using namespace std;

typedef long long LL;
typedef unsigned long long ULL;
typedef pair<int, int> PII;
typedef pair<double, double> PDD;
typedef __int128 LLL;

const double eps = 1e-8;
const double PI = acos(-1.0);
//const int N = , M = ;
const int INF = 0x3f3f3f3f;
const LL LINF = 1e18;
const double DINF = 1e20;
const int mod = 1e9 + 7;
int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};  //上右下左

int T, n, m, k, ans;

//邻接表
//int h[N], e[M], w[M], ne[M], idx;
//void add(int a, int b, int c){e[idx] = b, w[idx] = c, ne[idx] = h[a], h[a] = idx ++ ;}

int qmi(LL a, LL b, int p){int res = 1 % p;a %= p;while (b > 0){if(b & 1) res = (LL)res * a % p;a = (LL)a * a % p;b >>= 1;}return res;}
LL doMod(LL x, LL mod){return (x % mod + mod) % mod;}
int lowbit(int x){return x & -x;}
LL gcd(LL a, LL b) {return b?gcd(b,a%b):a;}

signed main()
{
    ios::sync_with_stdio(false), cin.tie(0), cout.tie(0);
    cout << fixed <<  setprecision(0);

    please ac;
}
/*
In every life we have some trouble． When you worry you make it double．
Don't worry，be happy．
                 .-~~~~~~~~~-._       _.-~~~~~~~~~-.
             __.'              ~.   .~              `.__
           .'//                  \./                  \`.
         .'//                     |                     \`.
       .'// .-~"""""""~~~~-._     |     _,-~~~~"""""""~-. \`.
     .'//.-"                 `-.  |  .-'                 "-.\`.
   .'//______.============-..   \ | /   ..-============.______\`.
 .'______________________________\|/______________________________`.
*/
```

